import React from 'react';
import { Grid, Card, CardContent, CardMedia, Typography, Button } from '@mui/material';
import styled from '@emotion/styled';

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  overflow: hidden;
  &:hover {
    transform: scale(1.05);
    transition: transform 0.2s;
  }
`;

const Media = styled(CardMedia)`
  height: 160px;
`;

const StyledButton = styled(Button)`
  margin-top: 16px;
  background-color: #40e0d0;
  color: #fff;
  &:hover {
    background-color: #00ced1;
  }
`;

const BookList = ({ books, addToReadingList }) => {
  return (
    <Grid container spacing={3}>
      {books.map((book, index) => (
        <Grid item xs={12} sm={6} md={4} lg={2} key={index}>
          <StyledCard>
            <Media image={book.coverPhotoURL} title={book.title} />
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p">
                by {book.author}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Reading Level: {book.readingLevel}
              </Typography>
              <StyledButton variant="contained" onClick={() => addToReadingList(book)}>
                Add to Reading List
              </StyledButton>
            </CardContent>
          </StyledCard>
        </Grid>
      ))}
    </Grid>
  );
};

export default BookList;
