import React from 'react';
import { List, ListItem, ListItemText, Button, Typography } from '@mui/material';

const ReadingList = ({ readingList, removeFromReadingList }) => {
  return (
    <>
      {readingList.length === 0 ? (
        <Typography variant="body1" color="textSecondary">
          Your reading list is empty.
        </Typography>
      ) : (
        <List>
          {readingList.map((book, index) => (
            <ListItem key={index} divider>
              <ListItemText primary={book.title} secondary={book.author} />
              <Button variant="contained" color="secondary" onClick={() => removeFromReadingList(index)}>
                Remove 
              </Button>
            </ListItem>
          ))}
        </List>
      )}
    </>
  );
};

export default ReadingList;
