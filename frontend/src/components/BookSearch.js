import React from 'react';
import { TextField } from '@mui/material';

const BookSearch = ({ searchTerm, setSearchTerm }) => {
  return (
    <TextField
      label="Search Books"
      variant="outlined"
      value={searchTerm}
      onChange={(e) => setSearchTerm(e.target.value)}
      fullWidth
      sx={{ marginBottom: 2 }}
    />
  );
};

export default BookSearch;
