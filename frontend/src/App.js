import React, { useState } from 'react';
import BookSearch from './components/BookSearch';
import BookList from './components/BookList';
import ReadingList from './components/ReadingList';
import { Container, Typography, Box, CircularProgress, Button } from '@mui/material';
import { ApolloProvider, useQuery, gql, client } from './apolloClient';
import styled from '@emotion/styled';

const BOOKS_QUERY = gql`
  query Books {
    books {
      author
      coverPhotoURL
      readingLevel
      title
    }
  }
`;

const Root = styled(Container)`
  font-family: 'Mulish', sans-serif;
  background-color: #fff;
  padding: 16px;
  border-radius: 10px;
`;

const LoadMoreButton = styled(Button)`
  margin: 32px 0;
  background-color: #40e0d0;
  color: #fff;
  &:hover {
    background-color: #00ced1;
  }
`;

const App = () => {
  const { loading, error, data } = useQuery(BOOKS_QUERY);
  const [searchTerm, setSearchTerm] = useState('');
  const [readingList, setReadingList] = useState([]);
  const [visibleBooks, setVisibleBooks] = useState(24);

  const addToReadingList = (book) => {
    if (!readingList.some((b) => b.title === book.title)) {
      setReadingList([...readingList, book]);
    }
  };

  const removeFromReadingList = (index) => {
    const newReadingList = [...readingList];
    newReadingList.splice(index, 1);
    setReadingList(newReadingList);
  };

  if (loading) return <CircularProgress />;
  if (error) return <Typography>Error loading books</Typography>;

  const filteredBooks = data.books.filter((book) =>
    book.title.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const handleLoadMore = () => {
    setVisibleBooks((prevVisibleBooks) => prevVisibleBooks + 24);
  };

  return (
    <Root>
      <img src="https://books.ello.com/static/media/logoEllo.2b20bb072a0c339867f3cb02fe3515b6.svg" alt="Ello Logo" width="54px" />
      <br />
      <br />
      <Typography variant="h6" gutterBottom color="primary">
        Book Assignment
      </Typography>
      <Box sx={{ marginBottom: 4 }}>
        <BookSearch searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
      </Box>
      <Typography variant="h5" gutterBottom color="text.primary">
        Search Results
      </Typography>
      <BookList books={filteredBooks.slice(0, visibleBooks)} addToReadingList={addToReadingList} />
      {visibleBooks < filteredBooks.length && (
        <LoadMoreButton variant="contained" onClick={handleLoadMore}>
          Load More
        </LoadMoreButton>
      )}
      <Typography variant="h5" gutterBottom color="text.primary" sx={{ marginTop: 4 }}>
        Reading List
      </Typography>
      <ReadingList readingList={readingList} removeFromReadingList={removeFromReadingList} />
    </Root>
  );
};

const WrappedApp = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);

export default WrappedApp;
