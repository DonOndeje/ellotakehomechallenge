import { ApolloClient, InMemoryCache, ApolloProvider, gql, useQuery } from '@apollo/client';

const client = new ApolloClient({
  uri: 'http://localhost:4000/', // GraphQL endpoint
  cache: new InMemoryCache(),
});

export { client, gql, ApolloProvider, useQuery };
