import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  typography: {
    fontFamily: 'Mulish, Arial, sans-serif',
  },
  palette: {
    primary: {
      main: '#5ACCCC', // Turquoise
    },
    secondary: {
      main: '#FF4500', // Orange Red
    },
    background: {
      default: '#FFFFFF', // White
    },
    text: {
      primary: '#212121', // Dark text
      secondary: '#757575', // Light text
    },
    accent: {
      turquoiseLight: '#CFFAFA',
      teal: '#4AA088',
      yellowDark: '#FAAD00',
      orangePastel: '#FFE6DC', 
    },
  },
});

export default theme;
